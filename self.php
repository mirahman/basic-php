<?php

class Cricketer {
    
    public $name;
    public $type;
    public $run;
    private $email;
    private $phone;
    protected $address;
    public $nationality;

    
    public function __construct($name = "", $type = "") {
        //echo "Yes i am now in construction mode <br />";
        //echo $name." > ".$type."<br />";
        
        $this->name = $name;
        $this->type = $type;
        $this->run = 0;
        $this->nationality = "Bangladeshi";
        $this->phone = "0171xxxxxxxx";
    }
    
    
    public  function setName($name) {
        $this->name = $name;
        
    }
    
    public final function getName() {
        return "<br />|| ".$this->name;
    }
    
    public function addRun($run) {
        $this->run += $run;
    }
    
    public function getRun() {
        return $this->run;
    }
    
    private function getPhone() {
        return $this->phone;
    }
    
    public function setPhone($phone) {
        $this->phone = $phone;
    }
    
    public function getPrvtPhone() {
        return $this->getPhone();
    }
    
    public function withoutThis() {
        return "I am without this";
    }
    
    public static function showMeSomething() {
        echo "Yes I can be accessed without any object ".self::withoutThis();
    }
}

Cricketer::showMeSomething();

$mash = new Cricketer('mash');
$mash->showMeSomething();