<?php

class Footballer {
    
    public $club;
    
    var $goal;
    
    public function __construct($club) {
       $this->club = $club;
    }
}

$messi = new Footballer('Barcelona');
$ronaldo = new Footballer("Real Madrid");

function doSum(Array $a, Footballer $b) {
   echo "I know you are from ".$b->club;
}

// since php 7.0

function doMoreSum(int $a, float $b, string $c, boolean $d) {
    
}

$a = 10;
$b = "str";

doSum([],$ronaldo);