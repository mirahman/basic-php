<?php
/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../Classes/');

/** PHPExcel */
include 'excel/PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'excel/PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
echo date('H:i:s') . " Create new PHPExcel object\n";
$objPHPExcel = new PHPExcel();
/*
// Set properties
echo date('H:i:s') . " Set properties\n";
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");
*/

// work starts from here

// Add some data
echo date('H:i:s') . " Add some data\n";
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'DOB');


for($i = 2;$i<100;$i++) {
    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, 'ID'.$i);
    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, 'Name'.$i);
    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $i);
}


$objPHPExcel->getActiveSheet()->setCellValue('C102', '=SUM(C2:C99)');
// Rename sheet

echo date('H:i:s') . " Rename sheet\n";


$objPHPExcel->createSheet();

$activeSheet = $objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('Rajni');

for($x = 0;$x<100;$x++)
  for($y = 0;$y<100;$y++)
    $activeSheet->setCellValueByColumnAndRow($y, $x, "Cell $x, $y");

 
// Save Excel 2007 file
echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

// Echo done
echo date('H:i:s') . " Done writing file.\r\n";