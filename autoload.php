<?php
/*
include "class/Foo.php";
include "class/AB.php";
include "class/XZ.php";
include "class/YZ.php";

$foo = new Foo;
$ab = new AB;
$xz = new XZ;
$yz = new YZ;
*/

/* ----------------- */

function __autoload($class_name) {
    echo "I am receiving ".$class_name."<br />";
    include_once "./class/".$class_name.".php";
}

//$foo = new Foo;
$ab = new AB;
$xz = new XZ;
$yz = new YZ;

