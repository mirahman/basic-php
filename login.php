<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php

session_start();

$token = sha1("new_user_login_".time().rand(1000,200000));

$_SESSION['_token'] = $token;

?>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    </head>
    <body>
        <div>Basic Login form</div>
        
        <form method="POST" id='form' action="dologin.php" enctype='multipart/form-data'>
            User Name:<br />
            <input type="text" name="name" value="" />
            <br />
            
           Password<br />
            <input type="password" name="password" value="" />
            <br />
            
            
            <input type="submit" value="Login" />
            
            <input type='button' value='Ajax Submit' onclick='ajaxSubmit()' />
            
            <input type="hidden" name="_token" value="<?php echo $_SESSION['_token']?>" />
            
            
        </form>
        
        <div id='showMe'>
            
        </div>
        
        <script>
            function ajaxSubmit() {
                console.log('calling');
            $.ajax({
  type: "POST",
  url: "process.php",
  data: $("#form").serialize(),
  success: function(data) {
      $("#showMe").html(data);
      
  }
});

            }
            
        </script>
    </body>
</html>
