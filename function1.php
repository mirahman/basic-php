<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

declare(strict_types=1);

function findNumber(int $num, Array $arr= []) :boolean {
    foreach($arr as $key => $value) {
        if($value == $num) return TRUE;
    }
    
    return FALSE;
}

$a = range(10,20);

$num = 9;


if(findNumber($num, 10)) {
    echo "Found";
} else {
    echo "Not Found";
}

