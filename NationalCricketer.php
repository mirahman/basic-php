<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NationalCricketer {
    
    public $name;
    public $type;
    public $run;

    
    public function __construct($name, $type = "") {
        echo "Yes i am now in construction mode <br />";
        //echo $name." > ".$type."<br />";
        
        $this->name = $name;
        $this->type = $type;
        $this->run = 0;
    }
    
    public function __get($name) {
        echo "I do not have ".$name."<br />";
    }
    
    
    
    public function __call($name,$args) {
        echo "No method found - ".$name."<br />";
    }
    
    public function setName($name) {
        $this->name = $name;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function addRun($run) {
        $this->run += $run;
    }
    
    public function getRun() {
        return $this->run;
    }
}


$mash  = new NationalCricketer("Mashrafee","Bowler");
$sakib = new NationalCricketer("Sakib","All Rounder", 100);
$mushy = new NationalCricketer("Mushy");

echo $mushy->totalRun;

echo $mash->getTotalRun();

echo $sakib->name;

$sakib->setName("Sakib Al Hasan");

$sakib->addRun(100);
$sakib->addRun(47);

echo $sakib->getRun();