<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DivideByZeroException extends Exception {};


// this is where i start
$flag = "Dev_start";

$debug = 0;

if($debug) print_r($something);

try {
    $a = 0;

    $b = 20;
    
    if($a > 0)
        echo $b/$a;
    else 
        throw new DivideByZeroException("Only positive number.");
    
    echo "Yes exception was handled";
    
    
} catch (DivideByZeroException $mm) {
    print_r($mm);
    echo "An exception occurred:<br />Message: ".$mm->getMessage()."<br />Error Code: ".$mm->getCode()." at ".$mm->getLine();
} catch (Exception $mnm) {
    echo "Eureka I got an exception";
} finally {
    echo "Done with try and catch";
}

echo "Test is done";
