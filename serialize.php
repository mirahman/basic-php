<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cricketer {
    
    public $name;
    public $type;
    public $run;
    private $email;
    private $phone;
    protected $address;
    public $nationality;

    
    public function __construct($name = "", $type = "") {
        //echo "Yes i am now in construction mode <br />";
        //echo $name." > ".$type."<br />";
        
        $this->name = $name;
        $this->type = $type;
        $this->run = 0;
        $this->nationality = "Bangladeshi";
        $this->phone = "0171xxxxxxxx";
    }
    
    
    public function setName($name) {
        $this->name = $name;
        
    }
    
    public function getName() {
        return "<br />|| ".$this->name;
    }
    
    public function addRun($run) {
        $this->run += $run;
    }
    
    public function getRun() {
        return $this->run;
    }
    
    private function getPhone() {
        return $this->phone;
    }
    
    public function setPhone($phone) {
        $this->phone = $phone;
    }
    
    public function getPrvtPhone() {
        return $this->getPhone();
    }
}

class NationalCricketer extends Cricketer{
    public $nationalLeagueMatcheCount;
    
    public function getName() {
        return "<br />From National Cricketer: ".parent::getName();
    }
}

class InternationalCricketer extends NationalCricketer {
    public $internationalT20Matches;
    
    public function getName() {
        return "<br />From International Cricketer: ".parent::getName();
    }
}

$kuddus = new Cricketer('Quddus');
$newkuddus = new Cricketer('Quddus');

$sharif = new NationalCricketer('Mohd. Sharif');
$sakib = new InternationalCricketer('Sakib al Hasan');

$arr = range(1,10);

echo serialize($arr);

$str = serialize($sharif);

$obj = unserialize($str);

echo $obj->getName();

$strJson = json_encode($sharif);

$newObj = json_decode($strJson);

print_r($newObj);

echo $newObj->getName();
