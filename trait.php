<?php

trait Utilities {
    public $name;
    
    public function getName() {
        echo "My name is khan";
    }
}

trait Security {
    
    public function validateHTML() {
        
    }
}

class Mizan {
    
    use Utilities, Security;
    
    public function getName() {
        echo "My name is mizan";
    }
}

$mizan = new Mizan();

echo $mizan->getName();