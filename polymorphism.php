<?php

abstract class Animal {
    
    abstract function speak() ;
    
}

class Dog extends Animal {
    
    public function speak() {
        echo 'Gheu Gheu';
    }
}

class Cat extends Animal {
    
    public function speak() {
        echo 'Meau Meau';
    }
}

class Goru extends Animal {
    
    public function speak() {
        echo 'Hamba Hamba';
    }
}

$objs = [new Dog(), new Cat(), new Goru()];

foreach($objs as $obj) {
    echo $obj->speak()."<br />";
}